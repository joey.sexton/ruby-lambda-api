require 'logger'

module LogLevels
  DEBUG = 'debug'
  INFO = 'info'
  WARN = 'warn'
  ERROR = 'error'
  FATAL = 'fatal'
end

$logLevel = ENV.fetch('LOG_LEVEL', 'info');
$logger = Logger.new($stdout)

def debug(*args)
  if [LogLevels::DEBUG].include?($logLevel)
    $logger.debug(*args)
  end
end

def info(*args)
  if [LogLevels::DEBUG, LogLevels::INFO].include?($logLevel)
    $logger.info(*args)
  end
end

def warn(*args)
  if [LogLevels::DEBUG, LogLevels::INFO, LogLevels::WARN].include?($logLevel)
    $logger.warn(*args)
  end
end

def error(*args)
  if [LogLevels::DEBUG, LogLevels::INFO, LogLevels::WARN, LogLevels::ERROR].include?($logLevel)
    $logger.error(*args)
  end
end

def fatal(*args)
  if [LogLevels::DEBUG, LogLevels::INFO, LogLevels::WARN, LogLevels::ERROR, LogLevels::FATAL].include?($logLevel)
    $logger.fatal(*args)
  end
end
