require 'json'
load 'log.rb'
load 'http/routes.rb'
load 'http/response.rb'

def lambda_handler(event:, context:)
  response = json(code: 500, error: 'Internal Server Error')

  begin
    object = $routes.fetch(event["path"], nil)
    if object != nil
      controller = object.new
      if controller.respond_to? event['httpMethod']
        response = controller.public_send(event['httpMethod'], event: event, context: context)
      else
        response = json(code: 400, error: 'Bad Request')
      end
    else
      response = json(code: 404, error: 'Not Found')
    end
  rescue
    error('Unable to make request.')
  end

  response
end
