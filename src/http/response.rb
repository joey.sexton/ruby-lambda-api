require 'json'

def json(code: 500, data: nil, error: nil)
  {
    statusCode: code,
    data: JSON.generate(data),
    error: JSON.generate(error),
  }
end
