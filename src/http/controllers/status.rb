load 'http/response.rb'

class StatusController
  def GET(event:, context:)
    data = "Success full request: #{event} #{context}"

    json(code: 200, data: data);
  end
end
